//for EN
const email = document.getElementById('en__field_supporter_emailAddress');
const phone = document.getElementById('en__field_supporter_phoneNumber');
if (email !== null && phone !== null) {
    email.setAttribute('type', 'email');
    phone.setAttribute('type', 'tel');
}

$('.en__field--text').each(function() {
    $(this).find('.en__field__input--text')
        .attr(
            'placeholder',
            $(this).children('.en__field__label').html()
        );
});

$('.en__field__input--select').append($('<option>', {
    value: '',
    text: '出生年份'
}));
for (i = new Date().getFullYear(); i > 1900; i--) {
    $('.en__field__input--select').append($('<option>', {
        value: '01/01/' + i.toString(),
        text: i
    }));
}

$('.en__field__phoneExample').insertAfter($('#en__field_supporter_phoneNumber'));

$('.en__field--email-ok-taiwan > label').html($('.en__field__label--item').text().replace('隱私保護政策', '<a href="http://www.greenpeace.org/taiwan/aboutus/privacy/">隱私保護政策</a>'));

//.en__field
//.en__field--emailAddress

let collapseForm = function() {
    $('.enform__default').hide();
    $('.en__field').hide();
    $('.en__field--emailAddress').show();
    $('.enform__text').show();
    $('.barWrapper').show();
};
let expandForm = function() {
    $('.enform__default').show();
    $('.en__field').show();
    $('.enform__text').hide();
    $('.barWrapper').hide();
};

$('#en__field_supporter_emailAddress, .en__submit').on('click', function() {
    expandForm();
});
$('.enform__close').on('click', function() {
    collapseForm();
});

// $('.mobile-botton').on('click', function() {
//     $('html, body').animate({
//         scrollTop: $('.enform').offset().top
//     }, 300);
//     //expandForm();
// });

//
/* ******************************************* */
/* CUSTOM VARIABLES - CHANGE FOR YOUR CAMPAIGN */
/* ******************************************* */
/* Set the Identifier for the Ticket HTML Element */
// This is the URL the page uses to pull the signatures from
// var counterBaseUrl = 'https://act.greenpeace.org/page/widget/399755';
// var tickerElementId = '#ticker';

/* ***************************** */
/* START OF SCRIPT - DO NOT EDIT */
/* ***************************** */
/* Get Petition Count */
// var counter = $('.counter');
// $.ajax({
//     type: 'GET',
//     url: counterBaseUrl,
//     dataType: 'json',
//     success: function(response) {
//         var obj = jQuery.parseJSON(response.jsonContent);
//         ticker(obj.initial, obj.goal, tickerElementId);
//     }
// });

// function numberWithCommas(x) {
//     return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
// }

// function ticker(n, g, identifier) {
//     /* Update the value */
//     $('.counter').text(n);
//     var count = n;
//     var target = g;
//     var percent = (count / target) * 100;
//     var percentage = $('.progress-bar').attr('aria-valuenow');
//     $('.progress-bar').width(percent + '%');
//     $('#numTarget').html(numberWithCommas(100000));
//     $(identifier).text(n);
//     $(identifier).each(function() {
//         $(this)
//             .prop('Counter', 0)
//             .animate({
//                 Counter: $(this).text()
//             }, {
//                 duration: 1600,
//                 easing: 'swing',
//                 step: function(now) {
//                     $(this).text(Math.ceil(now));
//                 },
//                 /* Add comma */
//                 complete: function() {
//                     $(identifier).text(numberWithCommas(n));
//                 }
//             });
//     });
// }
//slider
/*
var imgs = ['http://ddc.greenpeace.org.tw/2019/retailer/imgs/retailer_2.jpg', 'http://ddc.greenpeace.org.tw/2019/retailer/imgs/retailer_1.jpg'];
var ind = 0;
var slide = $(".slide-desktop");

if (document.body.clientWidth <= 750) {
    slide = $(".slide-mobile");
}
setInterval(function() {
    ind = ind % 2;
    slide.fadeOut(800, function() {
        slide.attr("src", imgs[ind++]);
        console.log(ind);
    }).fadeIn(800);
}, 4000);
*/
//utm
// function get_para(name, url) {
//     if (!url) url = location.href;
//     name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
//     var regexS = "[\\?&]" + name + "=([^&#]*)";
//     var regex = new RegExp(regexS);
//     var results = regex.exec(url);
//     return results == null ? null : results[1];
// }
// if (typeof(Storage) !== "undefined" && get_para("utm_campaign", location.href) != null) {
//     sessionStorage.utm = "utm_campaign=" + get_para("utm_campaign", location.href) +
//         "&utm_source=" + get_para("utm_source", location.href) +
//         "&utm_medium=" + get_para("utm_medium", location.href) +
//         "&utm_content=" + get_para("utm_content", location.href);
// }